﻿define me = Character("Me")
define what = Character("???")
define sm = Character("Shmik")

label start:
    play music "./audio/Elevator13.mp3"
    scene bg black
    "Darkness"
    scene bg white
    "Then light"
    "Something warm in the ear"
    "Buzzing and ringing"
    what "Nice, the last one did not have ears"
    what "... for the record, this work hurts all of my 3 angles..."
    what "Eyes?"
    show bg main blurred
    "something gently opens my eyes"
    what "Looks functional but dirty"
    "Something blinks my eyes"
    show bg main
    what "Here we go"
    what "Mouth?"
    "Something opens my mouth"
    "A strong chest push makes me exhale and produce a noise"
    what "Kind of"
    
    menu:
       what "Can speak?"
       "Yes":
          "Good."
       "No":
          "*Really?*"
    sm "I am Shmik"
    sm "And you are a random person"
    sm "Welcome"
    sm "For my thesis, i want you to answer questions"
    menu:
        sm "what is the last thing you remember?"
        "Playing a game in the browser":
                jump browser
        "Running a downloaded game":
                jump downloaded
label browser:
        me "I opened a game on Itch."
        me "It was called Random Human Generator or something."
        me "Clicked play in browser and here i am."
        jump go_on
label downloaded:
        me "I downloaded some RenPy game."
        me "Random Human Generator was the name i believe."
        me "Then i run it and i am here."
        jump go_on
label go_on:
        me "Also still playing it."
        sm "WOW... What a strange perception of reality!"
        sm "Turn around."
        show bg printer
        sm "The generator is truly awesome!"
        sm "I have seen people who think they died before!"
        sm "I have seen one who thinks they are a colourful piece of schlorg!"
        sm "And now a person who perceives the reality as a game!"
        sm "And all of this by randomizing genes and brain maps in the body-file to print!"
        sm "... most of them insta-die, but I must be lucky today."
        me "wait! Most of them die?"
        sm "Ah sorry, yep, the chances you have a longterm functional body are..."
        show bg black
        stop music
        "...darkness again... END"
        "...darkness again... END?"
        "...darkness again... END??"
        play music "./audio/Elevator13.mp3"
        show bg white
        "...light "
        "Something warm in the ear"
        "Buzzing and ringing"
        what "Nice, this one has one head..."
        what "... for the record, this work hurts all of my fluffy curves..."
        "..."
        return
