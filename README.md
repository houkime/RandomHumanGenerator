## Random Person Generator

It is a short visual novel using [RenPy](https://www.renpy.org/).

## To play
Get renpy (see link above), cd into the folder and call
```
renpy .
```

## Credits:
Coding, story, visuals: Houkime
Music: Elevator 13 by Stevia Sphere. CC-BY 3.0 https://creativecommons.org/licenses/by/3.0/
Got from here: https://open.audio/library/tracks/74829/
